package hocine.triki;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import hocine.triki.bean.MonBean;

@Path("/monbean")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ResourceMonBean {

	private Set<MonBean> mesBeans = Collections.newSetFromMap(Collections.synchronizedMap(new LinkedHashMap<>())); 
			//new HashSet<>();

	public ResourceMonBean() {
		mesBeans.add(new MonBean("Yo", "Yo", 50));
		mesBeans.add(new MonBean("Tyson", "Mike", 58));
	}
	
    @GET
    public Set<MonBean> list() {
    	return this.mesBeans;
    }
}
