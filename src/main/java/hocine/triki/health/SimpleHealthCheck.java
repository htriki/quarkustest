package hocine.triki.health;

import javax.enterprise.context.ApplicationScoped;

import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;
import org.eclipse.microprofile.health.Liveness;


// Accessible depuis l'adresse : /health/live
// Indique juste si l'application répond ou pas
@Liveness
@ApplicationScoped
public class SimpleHealthCheck implements HealthCheck {

	// Simple Interface qui retourne une réponse
	@Override
	public HealthCheckResponse call() {
		return HealthCheckResponse.up("Simple health check");
	}

}
