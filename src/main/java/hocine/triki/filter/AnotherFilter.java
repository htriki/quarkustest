package hocine.triki.filter;

import java.io.IOException;

import javax.annotation.Priority;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Priority(1)
@Provider
public class AnotherFilter implements ContainerRequestFilter {

	private static final Logger LOG = LoggerFactory.getLogger("AnotherFilter");
	
	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		
		LOG.info("******** ****************** ********");
		LOG.info("********        test        ********");
		LOG.info("******** ****************** ********");
	}

}
