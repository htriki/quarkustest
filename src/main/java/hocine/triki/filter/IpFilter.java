package hocine.triki.filter;

import java.io.IOException;

import javax.annotation.Priority;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.vertx.core.http.HttpServerRequest;

@Priority(2)
@Provider
public class IpFilter implements ContainerRequestFilter {

	private static final Logger LOG = LoggerFactory.getLogger("IpFilter");
	
    @Context
    UriInfo info;

    @Context
    HttpServerRequest request;
	
	@Override
	public void filter(ContainerRequestContext context) throws IOException {

        final String method = context.getMethod();
        final String path = info.getPath();
        final String address = request.remoteAddress().toString();

        LOG.info("Requete {} {} provenant de l'IP {}", method, path, address);		
		
	}


}
