package hocine.triki;

import java.util.Optional;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.config.inject.ConfigProperty;

import hocine.triki.configuration.Configuration;

@Path("/hello")
public class MyResource {


	@ConfigProperty(name = "greeting.message", defaultValue = "Mauvaise configuration")
	private String message;

	@ConfigProperty(name = "greeting.suffix", defaultValue="!") 
	String suffix;

	@ConfigProperty(name = "greeting.name")
	Optional<String> name; 	
	
	
	@Inject
	Configuration configuration;	
	
	
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String hello() {
    	
        return "hello ! " + this.message + this.name.orElse(" name est vide");
    }
    
    /*
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String message() {
    	return this.configuration.getMessage();
    }*/
}